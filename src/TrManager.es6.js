/**
 * Created by KuYaki on 21.05.2018.
 */

class TrManager {
	constructor() {
		this.src = '';

		this.dictionary = {};
		this.loadedDictionaries = {};

		this.defaultFunction = (text) => { return text };
		this.parent = undefined;
		this.usedWords = {}
	}

	generateDictionary(useUnmetWords=false, saveToFile=true) {
		let resultDict = {};
		for (let key in this.usedWords) {
			if (this.usedWords.hasOwnProperty(key)) {
				let value = this.dictionary[key];
				value = value === undefined ? '' : value;
				resultDict[key] = value;
			}
		}
		if (useUnmetWords) {
			for (let key in this.dictionary) {
				if (this.dictionary.hasOwnProperty(key)) {
					resultDict[key] = this.dictionary[key];
				}
			}
		}
		if (saveToFile) {
			let resultString = '{\n';
			const sortedKeys = Object.keys(resultDict).sort((a, b) => {
				return resultDict[a] < resultDict[b];
			});
			for (let i = 0; i < sortedKeys.length; i++) {
				resultString +=
					'\t"' + sortedKeys[i] + '": "' + resultDict[sortedKeys[i]] + '"' +
					((i < sortedKeys.length-1) ? ',' : '')  + '\n';
			}
			resultString += '}\n';
			const labels = this.src === '' ? ['default_language.json'] : this.src.split('/');
			const filename = labels[labels.length-1];
			const blob = new Blob([resultString], { type: 'text/plain' });
			let anchor = document.createElement('a');
			anchor.download = filename;
			anchor.href = (window.webkitURL || window.URL).createObjectURL(blob);
			anchor.dataset.downloadurl = ['text/plain', anchor.download, anchor.href].join(':');
			anchor.click();
		}
		return resultDict;
	}

	loadDictionary(handler=undefined) {
		let xhr = new XMLHttpRequest();
		xhr.onreadystatechange = () => {
			if (xhr.readyState === 4)
			{
				if (xhr.status === 200)
				{
					try {
						this.loadedDictionaries[this.src] = JSON.parse(xhr.response);
						this.dictionary = this.loadedDictionaries[this.src];
						if (handler !== undefined) {
							handler(true);
						}
						if (this.parent !== undefined) {
							this.parent.forceUpdate();
						}
					}
					catch (e){
						console.log('Failed to load loacalization json from text:', xhr.response);
					}
				}
			}
		};
		xhr.open('GET', this.src, true);
		xhr.send();
	}

	set(src, handler=undefined) {
		let res = false;
		if (src !== this.src) {
			this.src = src;
			if (!this.loadedDictionaries.hasOwnProperty(src)) {
				this.loadDictionary(handler);
				res = true;
			}
			else {
				this.dictionary = this.loadedDictionaries[src];
				if (handler !== undefined) {
					handler(false);
				}
				if (this.parent !== undefined) {
					window.setTimeout(() => {
						this.parent.forceUpdate();
					}, 1);
				}
			}
		}
		return res;
	}

	setDefaultFunction(func) {
		this.defaultFunction = func;
	}

	setParent(parent) {
		this.parent = parent;
	}

	tr(text) {
		this.usedWords[text] = '';
		const translation = this.dictionary[text];
		return translation === undefined ? this.defaultFunction(text) : translation;
	}
}

// return it as singleton
export default new TrManager()
