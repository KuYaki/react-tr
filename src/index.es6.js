import TrManager from './TrManager.es6.js';

export const TranslationManager = TrManager;
export const tr = (text) => TrManager.tr(text);
