## Install
	
	$ npm install react-tr

## Import
```js
// use TranslateManager in language changing handler
import { TranslationManager } from 'react-tr'
// use tr in translatable components
import { tr } from 'react-tr'
```

## Usage

The minimum set of functions You need for translating is TranslationManager.set and tr,
but it's strongly recommended to use TranslationManager.setParent as well.

### Management

##### TranslationManager.set(src, handler=undefined)
```js
// put this code in language changing handler
// for example:
handleEngClick = () => {
    TranslationManager.set('my.domain.com/localization/eng.json', (flag) => {
        console.log('Was dictionary loaded from src?', flag);
    })
};
handleRusClick = () => {
    TranslationManager.set('my.domain.com/localization/rus.json', (flag) => {
        console.log('Was dictionary already loaded before?', (!flag));
    })
};
```

##### TranslationManager.setParent(parent)
```js
// put this code in component, you want to update, after dictionary loading
// for example:
class App extends Component {
    constructor(props) {
        super(props);
        TranslationManager.setParent(this);
    }
    
    // ...
}
```

##### TranslationManager.setDefaultFunction(function)
```js
// put this code somewhere before rendering translatable components
// this will change default function (by default it returns text as it is)
// for example:
class App extends Component {
    constructor(props) {
        super(props);
        TranslationManager.setDefaultFunction((text) => { return '' });
    }
    
    // ...
}
// with this code, all unknown sentences will become empty strings
```

##### TranslationManager.loadDictionary(handler=undefined)
```js
// you can use loadDictionary if you want to force reloading from src
// for example:
handleThatServerToldThatHisDictionaryHasBeenChanged = () => {
    TranslationManager.loadDictionary((flag) => {
        console.log('Was dictionary updated?', flag);
    })
};
```

##### TranslationManager.generateDictionary(useUnmetWords=false, saveToFile=true)
```js
// every time tr was called, it save the word to usedWords dictionary
// call generateDictionary, if you want to save file with all the words, you have met on your site
handleSpecialClick = () => {
    TranslationManager.generateDictionary();
};
// if you don't want to save file, or if you want to use not encountered words,
// that were presented in your downloaded dictionary, you can change corresponding flags:
handleSpecialClick = () => {
    console.log('This is my dictionary: ' + JSON.parse(TranslationManager.generateDictionary(true, false)));
};
// this function returns dictionary with encountered words as keys and translations for this keys (or '') as values
```

### Translating

Use this in your components:
```html
// just put your text in tr:
<div>{ tr('This is the first text') }</div>
<div>{ tr('This is the second text') }</div>
<div>{ tr('This is the first text') }</div>
```

Then create .json files and put them somewhere on your server (for example: /var/www/html/localization/)

Example of rus.json file:
```json
{
    "This is the first text": "Этот текст написан на великом и могучем",
    "This is the second text": "Этот текст написан на могучем и великом"
}
```

Example of eng.json file:
```json
{
    "This is the first text": "This is first text in english",
    "This is the second text": "This is second text in english"
}
```

If text is not presented in the dictionary or .json files are unreachable or can't be parsed by JSON.parse,
text will be passed out of tr function by default function.
For example, code below will show you "This is the third text, and it is not presented in the dictionary"
(if default function was not changed by TranslationManager.setDefaultFunction)
```html
<div>{ tr('This is the third text, and it is not presented in the dictionary') }</div>
```
